/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Date;

/**
 *
 * @author anhdu
 */
public class Product {
    
    private int id;
    private String name;
    private int product_category;
    private String description;
    private float baseprice;
    private float price;
    private float discount;
    private int quantity;
    private Date created_on;
    private int created_by;
    private Date modified_on;
    private int modified_by;
    private String content;

    public Product() {
    }

    public Product(int id, String name, int product_category, String description, float baseprice, float price, float discount, int quantity, Date created_on, int created_by, Date modified_on, int modified_by, String content) {
        this.id = id;
        this.name = name;
        this.product_category = product_category;
        this.description = description;
        this.baseprice = baseprice;
        this.price = price;
        this.discount = discount;
        this.quantity = quantity;
        this.created_on = created_on;
        this.created_by = created_by;
        this.modified_on = modified_on;
        this.modified_by = modified_by;
        this.content = content;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getProduct_category() {
        return product_category;
    }

    public void setProduct_category(int product_category) {
        this.product_category = product_category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getBaseprice() {
        return baseprice;
    }

    public void setBaseprice(float baseprice) {
        this.baseprice = baseprice;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public float getDiscount() {
        return discount;
    }

    public void setDiscount(float discount) {
        this.discount = discount;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Date getCreated_on() {
        return created_on;
    }

    public void setCreated_on(Date created_on) {
        this.created_on = created_on;
    }

    public int getCreated_by() {
        return created_by;
    }

    public void setCreated_by(int created_by) {
        this.created_by = created_by;
    }

    public Date getModified_on() {
        return modified_on;
    }

    public void setModified_on(Date modified_on) {
        this.modified_on = modified_on;
    }

    public int getModified_by() {
        return modified_by;
    }

    public void setModified_by(int modified_by) {
        this.modified_by = modified_by;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", product_category=" + product_category + ", description=" + description + ", baseprice=" + baseprice + ", price=" + price + ", discount=" + discount + ", quantity=" + quantity + ", created_on=" + created_on + ", created_by=" + created_by + ", modified_on=" + modified_on + ", modified_by=" + modified_by + ", content=" + content + '}';
    }
    
    
    
}
