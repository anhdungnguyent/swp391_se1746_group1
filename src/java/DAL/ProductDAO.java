/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAL;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Product;

/**
 *
 * @author anhdu
 */
public class ProductDAO {
    
    public List<Product> GetProducts() {
        List<Product> products = new ArrayList<>();
        String sql = "select * from product";
        try {
            ResultSet rs = DBConnect.getResultFromSqlQuery(sql);
            while (rs.next()) {
                Product p = new Product(rs.getInt("id"),
                                        rs.getString("name"), 
                                        rs.getInt("product_category"), 
                                        rs.getString("description"), 
                                        rs.getFloat("baseprice"), 
                                        rs.getFloat("price"), 
                                        rs.getFloat("discount"), 
                                        rs.getInt("quantity"), 
                                        rs.getDate("created_on"), 
                                        rs.getInt("created_by"), 
                                        rs.getDate("modified_on"), 
                                        rs.getInt("modified_by"), 
                                        rs.getString("content"));
                products.add(p);
            }
        }
        catch(SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return products;
    }
    
    public static void main(String[] args) {
        ProductDAO pd = new ProductDAO();
        List<Product> list = pd.GetProducts();
        System.out.println(list.get(0).getName());
    }
    
}
