/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAL;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author anhdu
 */
public class DBConnect {
    
    protected static Connection connection;

    public static Connection GetConnect() {
        try {
            String user = "root";
            String pass = "admin"; 
            String url = "jdbc:mysql://localhost:3306/fs_group1?zeroDateTimeBehavior=CONVERT_TO_NULL";
            Class.forName("com.mysql.cj.jdbc.Driver");
            System.setProperty("com.mysql.cj.jdbc.Driver","");
            connection = DriverManager.getConnection(url, user, pass);
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(DBConnect.class.getName()).log(Level.SEVERE, null, ex);
        }
        return connection;
    }
    
    public static void CloseConnection() {
        if (connection != null) {
            try {
                connection.close();
                connection = null;
            }
            catch (SQLException ex){
                
            }
        }
    }
    
    public static ResultSet getResultFromSqlQuery(String SqlQueryString){
        //Creating Resultset object
        ResultSet rs = null;
        try {
            //Checking whether the connection is null or null
            if (connection == null) {
                GetConnect();
            }
            //Querying the query
            rs = connection.createStatement().executeQuery(SqlQueryString);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return rs;
    }

    //Creating universal method to query for inserting or updating information in mysql database
    public static int insertUpdateFromSqlQuery(String SqlQueryString) throws ClassNotFoundException {
        int i = 2;
        try {
            //Checking whether the connection is null or null
            if (connection == null) {
                GetConnect();
            }
            //Querying the query
            i = connection.createStatement().executeUpdate(SqlQueryString);

        } catch (SQLException ex) {
        }
        return i;
    }
    
}
