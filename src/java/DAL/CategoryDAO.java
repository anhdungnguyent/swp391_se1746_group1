/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAL;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Category;

/**
 *
 * @author anhdu
 */
public class CategoryDAO {
    
    public List<Category> GetCategories(){
        List<Category> categories = new ArrayList<>();
        String sql = "select * from category";
        try {
            ResultSet rs = DBConnect.getResultFromSqlQuery(sql);
            while (rs.next()){
                Category c = new Category(rs.getInt("id"), 
                                        rs.getString("category_name"), 
                                        rs.getString("description"), 
                                        rs.getDate("created_on"), 
                                        rs.getInt("created_by"), 
                                        rs.getDate("modified_on"), 
                                        rs.getInt("modified_by"));
                categories.add(c);
            }
        }
        catch (SQLException ex){
            System.out.println(ex.getMessage());
        }
        return categories;
    }
    
}
